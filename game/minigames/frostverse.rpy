init python:
    import random
    import re

label frostverse_start:
    python:
        template = frostverse.templates[0]

        renpy.show("moroes idle")

        placeholders = re.findall(r'_(a|v|n)_', template)
        modifiers = {
            "insanity": 1
        }

        for ph in placeholders:
            random.shuffle(frostverse.words)
            wordPool = min(max(chars.moroes['stat_a'], 3), 10)
            picked = {
                "a": [w for w in frostverse.words if w["type"] == "a"][:wordPool],
                "v": [w for w in frostverse.words if w["type"] == "v"][:wordPool],
                "n": [w for w in frostverse.words if w["type"] == "n"][:wordPool],
            }
            random.shuffle(frostverse.templates)

            prompt = re.sub(r'_(a|v|n)_', "___", template)
            prompt = re.sub(r'___', "{color=#ff0000}{b}[***]{/b}{/outlinecolor}", prompt, 1)
            narrator(
                "Помогите Ивану заполнить пропуски в слогане:\n\n{color=#e0e0a0}\"" + prompt + "\"{/color}...",
                 interact=False
            )

            menu_items = []

            for k, v in enumerate(picked[ph]):
                 menu_items.append((v["word"], v))

            choice = menu(menu_items)

            template = re.sub(r'_' + ph + '_', '{color=#ffff00}{b}' + choice["word"] + '{/b}{/b}', template, 1)

            if "effect" in choice:
                for k in choice["effect"].keys():
                    modifiers[k] = modifiers.get(k, 0) + choice["effect"][k]

        renpy.say(chars.m, "{i}\"" + template + "\"{/i}...", interact=False)

        phrases = bot.gpt(
            template,
            min(chars.moroes["insanity"], 5)
        )
        renpy.pause()

        for k, p in enumerate(phrases):
            fade = min(250, max(100, 250 - k * chars.moroes["insanity"] * 20))
            renpy.say(
                chars.m,
                (template + p) if k == 0 else p,
                what_color = (250, fade, fade, fade)
            )

        if len(modifiers) > 0:
            report = "По итогам решений изменились характеристики: "
            for k, v in modifiers.items():
                report += "(" + k + ": " + (("+" + str(v)) if v > 0 else str(v)) + ") "
                chars.moroes[k] = chars.moroes[k] + v

            renpy.say(chars.m, report)

    return