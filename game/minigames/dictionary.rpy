# типы: a - прилагательное, n - существительное, v - глагол
# эффекты: не ебу, пусть будут пока a b c
init python in frostverse:
    templates = [
        "Хочешь _a_ _v_? Вступай к нам! Мы дадим тебе _v_",
        "В этот столь непростой час наши сердца требуют _v_ и _v_!",
        "Каждому из нас когда-то хотелось _a_, _a_.. Однако ж получили лишь _v_"
    ]

    words = [
        {
            "type": "a",
            "word": "смазанного",
            "effect": {
                "stat_a": 1,
                "stat_b": -1
            }
        },
        {
            "type": "a",
            "word": "веселого"
        },
        {
            "type": "a",
            "word": "хмурого",
        },
        {
            "type": "a",
            "word": "всратого"
        },
        {
            "type": "a",
            "word": "удивительного",
            "effect": {
                "stat_c": -5,
                "stat_b": 1
            }
        },
        {
            "type": "a",
            "word": "чарующего"
        },
        {
            "type": "a",
            "word": "скрипучего"
        },
        {
            "type": "a",
            "word": "шелушащегося"
        },
        {
            "type": "a",
            "word": "неуместного"
        },
        {
            "type": "a",
            "word": "невероятного"
        },
        {
            "type": "a",
            "word": "вероятного"
        },
        {
            "type": "a",
            "word": "совиного"
        },
        {
            "type": "a",
            "word": "усатого",
            "effect": {
                "stat_a": 1,
                "stat_b": 1,
                "stat_c": 1
            }
        },
        {
            "type": "a",
            "word": "рыбного"
        },
        {
            "type": "a",
            "word": "жестокого"
        },
        {
            "type": "a",
            "word": "задорного"
        },
        {
            "type": "a",
            "word": "привычного"
        },
        {
            "type": "a",
            "word": "отбитого",
            "effect": {
                "stat_a": -1,
                "stat_b": 1,
                "stat_c": -1
            }
        },
        {
            "type": "a",
            "word": "мистического"
        },
        {
            "type": "a",
            "word": "духовного"
        },
        {
            "type": "a",
            "word": "материального"
        },
        {
            "type": "a",
            "word": "материнского"
        },
        {
            "type": "a",
            "word": "системного"
        },
        {
            "type": "a",
            "word": "гадкого"
        },
        {
            "type": "a",
            "word": "гладкого"
        },
        {
            "type": "a",
            "word": "гладенького"
        },
        {
            "type": "a",
            "word": "струящегося"
        },
        {
            "type": "a",
            "word": "смеюзщегося"
        },
        {
            "type": "a",
            "word": "питательного"
        },
        {
            "type": "a",
            "word": "обаятельного",
            "effect": {
                "stat_c": 1
            }
        },
        {
            "type": "a",
            "word": "объемного",
            "effect": {
                "stat_a": 3,
            }
        },
        {
            "type": "a",
            "word": "здорового"
        },
        {
            "type": "a",
            "word": "голодного"
        },
        {
            "type": "a",
            "word": "сытого"
        },
        {
            "type": "a",
            "word": "летающегося"
        },
        {
            "type": "a",
            "word": "переливающегося"
        },
        {
            "type": "a",
            "word": "перломутрового"
        },
        {
            "type": "a",
            "word": "мышиного"
        },
        {
            "type": "a",
            "word": "изогнутого",
            "effect": {
                "stat_b": -2
            }
        },
        {
            "type": "a",
            "word": "танцующего"
        },
        {
            "type": "a",
            "word": "пульсирующего"
        },
        {
            "type": "a",
            "word": "развивающегося"
        },
        {
            "type": "a",
            "word": "деградирующего"
        },
        {
            "type": "a",
            "word": "аутичного",
            "effect": {
                "stat_a": 3,
                "stat_b": 2,
                "stat_c": 1
            }
        },
        {
            "type": "a",
            "word": "дебильного"
        },
        {
            "type": "a",
            "word": "густого"
        },
        {
            "type": "a",
            "word": "пустого"
        },
        {
            "type": "a",
            "word": "утомленного",
            "effect": {
                "stat_a": -1,
                "stat_b": 1,
                "stat_c": 10
            }
        },
        {
            "type": "a",
            "word": "тонизирующего"
        },
        {
            "type": "a",
            "word": "бодрящего"
        },
        {
            "type": "a",
            "word": "наркотического"
        },
        {
            "type": "a",
            "word": "упоротого",
            "effect": {
                "stat_c": 1
            }
        },
        {
            "type": "a",
            "word": "ебнутого",
            "effect": {
                "stat_a": 5,
            }
        },

        {
            "type": "v",
            "word": "конца"
        },
        {
            "type": "v",
            "word": "лица"
        },
        {
            "type": "v",
            "word": "торца"
        },
        {
            "type": "v",
            "word": "пиздеца"
        },
        {
            "type": "v",
            "word": "сияния"
        },
        {
            "type": "v",
            "word": "приключения"
        },
        {
            "type": "v",
            "word": "животного"
        },
        {
            "type": "v",
            "word": "просветления"
        },
        {
            "type": "v",
            "word": "подъема"
        },
        {
            "type": "v",
            "word": "забвения"
        },
        {
            "type": "v",
            "word": "алгоритма",
            "effect": {
                "stat_b": -1,
                "stat_c": 1
            }
        },
        {
            "type": "v",
            "word": "органа",
            "effect": {
                "stat_a": 1,
                "stat_b": 1,
                "stat_c": 1
            }
        },
        {
            "type": "v",
            "word": "пресыщения"
        },
        {
            "type": "v",
            "word": "последствия"
        },
        {
            "type": "v",
            "word": "отростка",
            "effect": {
                "stat_c": -1
            }
        },
        {
            "type": "v",
            "word": "хуйца"
        },
        {
            "type": "v",
            "word": "аромата",
            "effect": {
                "stat_a": 1,
                "stat_b": 1,
                "stat_c": 1
            }
        },
        {
            "type": "v",
            "word": "знания"
        },
        {
            "type": "v",
            "word": "действия"
        },
        {
            "type": "v",
            "word": "умения",
            "effect": {
                "stat_a": -1,
                "stat_b": 2,
                "stat_c": 1
            }
        },
        {
            "type": "v",
            "word": "терпения"
        },
        {
            "type": "v",
            "word": "увожения",
            "effect": {
                "stat_a": 1,
                "stat_b": 1,
                "stat_c": 1
            }
        },
        {
            "type": "v",
            "word": "увлаждения",
            "effect": {
                "stat_a": 1,
                "stat_b": 1,
                "stat_c": 5
            }
        },
        {
            "type": "v",
            "word": "достижения"
        },
        {
            "type": "v",
            "word": "страдания"
        },
        {
            "type": "v",
            "word": "понимания"
        },
        {
            "type": "v",
            "word": "привыкания"
        },
        {
            "type": "v",
            "word": "заблуждения"
        },
        {
            "type": "v",
            "word": "осознания",
            "effect": {
                "stat_a": 1,
                "stat_b": 8,
                "stat_c": 1
            }
        },
        {
            "type": "v",
            "word": "здоровья"
        },
        {
            "type": "v",
            "word": "счастья"
        },
        {
            "type": "v",
            "word": "долголетия"
        },
        {
            "type": "v",
            "word": "омоложения",
            "effect": {
                "stat_a": 1,
                "stat_b": 1,
                "stat_c": -10
            }
        },
        {
            "type": "v",
            "word": "преображения"
        },
        {
            "type": "v",
            "word": "восхищения"
        },
        {
            "type": "v",
            "word": "украшения",
            "effect": {
                "stat_a": 11,
            }
        },
        {
            "type": "v",
            "word": "тигра"
        },
        {
            "type": "v",
            "word": "филина"
        },
        {
            "type": "v",
            "word": "александра",
            "effect": {
                "stat_b": -3,
                "stat_c": 1
            }
        },
        {
            "type": "v",
            "word": "сергея"
        },
        {
            "type": "v",
            "word": "матерка"
        },
        {
            "type": "v",
            "word": "трактора"
        },
        {
            "type": "v",
            "word": "облысения",
            "effect": {
                "stat_c": 1
            }
        },
        {
            "type": "v",
            "word": "рождения"
        },
        {
            "type": "v",
            "word": "осквернения",
            "effect": {
                "stat_a": 1,
                "stat_b": -1,
                "stat_c": -1
            }
        },
        {
            "type": "v",
            "word": "осуждения",
            "effect": {
                "stat_a": 2,
                "stat_b": 2,
                "stat_c": 2
            }
        },
        {
            "type": "v",
            "word": "извиненеия",
            "effect": {
                "stat_a": 1,
                "stat_b": -1,
                "stat_c": 1
            }
        },
        {
            "type": "v",
            "word": "хлеба"
        },
        {
            "type": "v",
            "word": "гниения"
        },
        {
            "type": "v",
            "word": "торможения"
        }

    ]