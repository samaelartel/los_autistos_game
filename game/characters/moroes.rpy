init 10 python in chars:
    def new_moroes():
        return pc(
            name="Мороз",
            color="#faf0be",
            stats={
                "stat_a": 5,
                "stat_c": 3,
                "insanity": 1
            }
        )

    moroes = new_moroes()

init:
    image moroes idle = "characters/frost_idle.png"
    define chars.m = Character("Moroes", image="moroes", color="#faf0be")

