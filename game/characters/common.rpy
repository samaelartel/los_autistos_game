init python in chars:
    class pc(object):
        def __init__(self, name="???", color="#d0d0d0", image="unknown", stats={}):
            self._name = str(name)
            self._color = color
            self._image = image
            self._stats = dict(stats)

        def __setitem__(self, key, val):
            self._stats[key] = val
        def __getitem__(self, key):
            return {
                "name": self._name,
                "color": self._color
            }.get(key, self._stats.get(key, 0))
