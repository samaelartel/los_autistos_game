init python in bot:
    import requests
    import re

    def gpt(txt, amount):
        txt = re.sub(r'\{\[\\a-z]\}', '', txt)
        response = requests.post(
            "https://models.dobro.ai/gpt2/medium/",
            json={"prompt":txt, "num_samples":amount, "length":40}
        )
        if response.status_code == 200 and response.json()["replies"][0]:
            return response.json()["replies"]
        else:
            return ["..."]
