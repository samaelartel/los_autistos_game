﻿init:
    define config.developer = True

label start:
    scene bg default
    play music "audio/collect.mp3"

label mainmenu:

    menu:
        "Помочь Морозу придумать слоган":
            call frostverse_start from _call_frostverse_start
            jump mainmenu
        "Нет, хватит":
            pass

    return
